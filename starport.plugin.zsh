install_ignite() {
    local install_dir="$HOME/.local/bin"

    if ! type ignite 2>&1 >/dev/null; then
        echo "--> Installing ignite at $install_dir/ignite"
        echo

        mkdir -p "$install_dir" 2>&1 >/dev/null
        pushd "$install_dir" 2>&1 >/dev/null

        curl https://get.ignite.com/cli | bash

        popd 2>&1 >/dev/null

        ignite version

        if [ -d "$ZSH_COMPLETIONS" ]; then
            echo "--> Installing ignite completions at $ZSH_COMPLETIONS/_ignite"
            ignite completion zsh >"$ZSH_COMPLETIONS/_ignite"
        fi
    fi

    if [ -f "$ZSH_COMPLETIONS/_ignite" ]; then
        source "$ZSH_COMPLETIONS/_ignite"
        compdef _ignite ignite
    fi
}

install_ignite
